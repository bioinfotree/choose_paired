# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>

# test
SEQ1 ?= 
SEQ2 ?= 

TARGET_PAIRS ?= 30000000

extern ../phase_1/not.joinable.tab as NOT_JOINABLE
extern ../phase_1/joinable.part.tab as JOINABLE

# number of pairs that are missing
MISSING_PAIRS := $(shell echo "$(TARGET_PAIRS) - $$(cat ../phase_1/joinable.part.tab | wc -l)" | bc  )




not.joinable.tab:
	ln -sf $(NOT_JOINABLE) $@


#$(call first,$(call split,.,$(notdir $(SEQ1)))).part.fastq.gz: joinable.tab sort.seq
not.joinable.part.pair.1.fastq.gz: not.joinable.tab
	head -n $(MISSING_PAIRS) <$< \
	| cut -f 1,2,3 \
	| tab2fastq \
	| gzip -c >$@

not.joinable.part.pair.2.fastq.gz: not.joinable.tab
	head -n $(MISSING_PAIRS) <$< \
	| cut -f 4,5,6 \
	| tab2fastq \
	| gzip -c >$@


not.joinable.part.pair.%_fastqc.html: not.joinable.part.pair.%.fastq.gz
	!threads
	$(call load_modules); \
	fastqc --threads $$THREADNUM $<

.PHONY: test
test:
	@echo $(MISSING_PAIRS)
	#echo $(call first,$(call split,.,$(notdir $(SEQ1)))).part.fastq.gz

# execute only if needed more then TARGET_PAIRS pairs
ifneq "$(MISSING_PAIRS)" "0"

ALL +=  not.joinable.part.pair.1.fastq.gz \
	not.joinable.part.pair.2.fastq.gz \
	not.joinable.part.pair.1_fastqc.html \
	not.joinable.part.pair.2_fastqc.html

INTERMEDIATE += 

CLEAN += not.joinable.tab \
	 $(wildcard *.zip)

endif