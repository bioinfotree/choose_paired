# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>

# test
SEQ1 ?= 
SEQ2 ?= 

TARGET_PAIRS ?= 

MIN_OVERLAP ?= 10
MAX_OVERLAP ?= 300

logs:
	mkdir -p $@


define normalize_header
	zcat $1 \
	| fastq2tab \
	| bawk '!/^[\#+,$$]/ { \
	split($$1,a," "); \
	split(a[1],b,"/"); \
	print b[1],$$2,$$3; \
	}' \
	| tab2fastq >$2
endef

pair.1.fastq:
	$(call normalize_header,$(SEQ1),$@)

pair.2.fastq:
	$(call normalize_header,$(SEQ2),$@)


.META: sort.seq
	1	lenght_of_R1+R2
	2	R1_header
	3	R1_sequence
	4	R1_quality
	5	R2_header
	6	R2_sequence
	7	R2_quality


.PRECIOUS: sort.seq
sort.seq: pair.1.fastq pair.2.fastq
	paste \
	<( cat $< | fastq2tab ) \
	<( cat $^2 | fastq2tab ) \
	| bawk '!/^[\#+,$$]/ { print length($$2$$5),$$0; }' \
	| sort -r -n >$@   * sort by total lenght of R1 + R2 *

.META: joinable.tab
	1	R1_&_R2_header

joinable.tab: logs sort.seq
	!threads
	$(call load_modules); \
	select_columns 2 3 4 6 7 <$^2 \
	| flash \
	 --min-overlap=$(MIN_OVERLAP) \   * minimum required overlap length between two reads. Default: 10bp *
	 --max-overlap=$(MAX_OVERLAP) \   * maximum required overlap length between two reads. Default: 65bp *
	 --max-mismatch-density=0.25 \   * Maximum allowed ratio between the number of mismatched base pairs and the overlap length. *
	 --threads=$$THREADNUM \
	 --to-stdout \
	 --tab-delimited-output \
	 --tab-delimited-input - \
	 2> $</flash.$@.log \
	 | bawk '!/^[\#+,$$]/ { \
	 split($$1,a," "); \
	 if ( NF == 3 ) { print a[1]; } \   * print joined sequences to target file. Cut header to first space. Needed by nt-fasta-extract *
	 else if ( NF == 5 ) { print a[1],$$2,$$3,a[1],$$4,$$5 >"not.$@"; } \   * print NOT joined sequences to other file. Cut headers to first space *
	 }' >$@



.META: not.joinable.tab
	1	R1_header
	2	R1_sequence
	3	R1_quality
	4	R2_header
	5	R2_sequence
	6	R2_quality

not.joinable.tab: joinable.tab
	touch $@



.META: joinable.part.tab
	1	R1_&_R2_header

joinable.part.tab: joinable.tab
	head -n $(TARGET_PAIRS) <$< >$@



joinable.part.pair.%.fastq.gz: pair.%.fastq joinable.part.tab
	$(call load_modules); \
	nt-fasta-extract --input $< --list $^2 \
	| gzip -c >$@

joinable.part.pair.%_fastqc.html: joinable.part.pair.%.fastq.gz
	!threads
	$(call load_modules); \
	fastqc --threads $$THREADNUM $<

.PHONY: test
test:
	


ALL +=  logs \
	pair.1.fastq \
	pair.2.fastq \
	joinable.part.tab \
	not.joinable.tab \
	joinable.part.pair.1.fastq.gz \
	joinable.part.pair.2.fastq.gz \
	joinable.part.pair.1_fastqc.html \
	joinable.part.pair.2_fastqc.html

INTERMEDIATE += 

CLEAN += sort.seq \
	 pair.1.fastq \
	 pair.2.fastq \
	 joinable.tab \
	 not.joinable.tab \
	 joinable.part.pair.1_fastqc.zip \
	 joinable.part.pair.2_fastqc.zip
